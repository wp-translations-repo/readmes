=== Divi French ===
Contributors: WP-Translations Team, G3ronim0, fxbenard
Tags: i18n, l10n, WP-T
Requires at least: 4.1
Tested up to: 4.9.2
Stable tag: 2.2.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

EDD Content Restriction French, la traduction française pour EDD Content Restriction


== Description ==

EDD Content Restriction French, la traduction française pour [EDD Content Restriction] (https://fxbenard.com/traduction/edd-content-restriction/). EDD Content Restriction French vous permet grâce à son système de licence et de Language Pack intégrés de bénéficier en toute tranquilité et liberté des mises à jour des traductions.

**Related Links:**

* https://wp-translations.store/fr/


== Support ==

Pour toutes suggestions, améliorations ou demandes de corrections, n’hésitez pas à utiliser [notre page de contact](https://wp-translations.store/fr/contact/).
Ou directement par e-mail : wpt@wp-translations.store.


== Installation ==

1. Téléchargez le fichier zip de l’extension.
2. Installez et activez l’extension en cliquant sur via « Extensions » > « Ajouter ».
3. Saississez et activez votre licence dans le menu « WPT Pro » > « EDD Content Restriction French ».
4. Voilà, votre produit WordPress est traduit et prêt à recevoir ses prochaines mises à jour de traductions !


== Changelog ==

= 2.2.6 - 2018-01-31 =
* Refonte pour WP-Translations Store
* Mise à jour intégrale

= 2.0.8 - 2015-07-30 =
* Numéro version identique à EDD Content Restriction

= 1.0.0 - 2015-07-27 =
* Disponible

= 0.0.1 2015-07 =
* Le code est tout chaud