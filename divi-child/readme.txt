=== Divi French ===
Contributors: fxbenard, G3ronim0
Tags: français, fxbenard, Divi
Requires at least: 4.1
Tested up to: 4.6.1
Stable tag: 3.0.19
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Divi French est une extension de traduction pour Divi

== Description ==

Divi French est une extension de traduction française pour [Divi](https://fxbenard.com/recommande/divi/). Elle vous permet d’utiliser Divi-Child en toute liberté et de pouvoir bénéficier de la mise à jour des traductions.

**Related Links:**

* https://www.fxbenard.com/


== Support ==

Pour toutes suggestions, améliorations ou demandes de corrections, n’hésitez pas à utiliser [ma page de contact](https://fxbenard.com/contactez-moi/).
Ou directement par e-mail : fx@fxbenard.com.


== Installation ==

1. Télécharger le fichier zip de l’extension
2. Installer l’extension en cliquant sur via "Extensions" > "Ajouter".
3. Activer votre licence dans le menu "FX Trads" > "Divi French".
4. C’est tout, votre thème est traduit !


== Changelog ==

= 3.0.19 =
* Sortie : 18 novembre 2016
* Mise à jour pour Divi 3.0.19
* Correction traduction des réglages du thème

= 3.0.15 =
* Sortie : 20 octobre 2016
* Mise à jour pour Divi 3.0.15
* Refonte traduction des réglages du thème
* Simplification de la personnalisation

= 3.0.14 =
* Sortie : 12 octobre 2016
* Mise à jour pour Divi 3.0.14

= 3.0.9 =
* Sortie : 28 septembre 2016
* Mise à jour pour Divi 3.0.9

= 3.0.8 =
* Sortie : 25 septembre 2016
* Mise à jour pour Divi 3.0.8
* Ajout traductions Builder Visuel

= 3.0.7 =
* Sortie : 20 septembre 2016
* Mise à jour pour Divi 3.0.7
* Amélioration des règles CSS

= 3.0.6 =
* Sortie : 18 septembre 2016
* Mise à jour pour Divi 3.0.6
* Ajout CSS pour meilleure affichage du Builder

= 3.0.5 =
* Sortie : 16 septembre 2016
* Mise à jour pour Divi 3.0.5
* Amélioration traductions A/B Section
* Retour des majuscules sur les modules

= 3.0.1 =
* Sortie : 09 septembre 2016
* Mise à jour pour Divi 3.0.1

= 3.0.0 =
* Sortie : 07 septembre 2016
* Mise à jour pour Divi 3.0.0 - WordPress 4.6.1
* Amélioration traductions (props @nicolasricher)

= 2.7.8 =
* Sortie : 06 juillet 2016
* Mise à jour pour Divi 2.7.8 - WordPress 4.5.3
* Amélioration traductions (props @nicolasricher)

= 2.7.5.1 =
* Sortie : 27 mai 2016
* Correction du problème d’enregistrement dans l’outil de personnalisation

= 2.7.5 =
* Sortie : 19 mai 2016
* Mise à jour pour Divi 2.7.5 - WordPress 4.5.2
* Correction du bug du non chargement des trads du Builder
* Refonte complète des traductions du Builder - Termes officiels de WordPress
* Correction typographiques - Espaces insécables
* Suppression de la traduction de et_automatic_updates
* Traduction onglet mises à jour

= 2.7.3 =
* Sortie : 3 mai 2016
* Mise à jour pour Divi 2.7.3

= 2.7.1 =
* Sortie : 4 avril 2016
* Mise à jour pour Divi 2.7.1

= 2.7.0 =
* Sortie : 1 avril 2016
* Mise à jour pour Divi 2.7
* Amélioration - Traductions personnalisées dans  WP_LANG_DIR/fx-trads/

= 2.6.4.4 =
* Sortie : 29 mars 2016

* Mise à jour  pour Divi 2.6.4.4
* Amélioration - Numéro de  licence masqué dans l’interface
* Amélioration traductions du e-Panel (props @nicolasricher)

= 2.6.4.2 =
* Sortie : 22 février 2016

* Mise à jour  pour Divi 2.6.4.2
* Insertion traduction pour Elegant Themes updater

= 2.6.4.1 =
* Sortie : 21 février 2016

* Mise à jour  pour Divi 2.6.4.1

= 2.6.1 =
* Sortie : 27 janvier 2016

* Mise à jour  pour Divi 2.6.1

= 2.6.0 =
* Sortie : 26 janvier 2016

* Mise à jour traduction pour Divi 2.6.0
* +200 nouvelles traductions du Builder/Divi
* Refonte interface merci @G3ronim0
* Testé pour WP 4.4.1

= 2.5.5 =
* Sortie : 11 novembre 2015

* Mise à jour traduction pour Divi 2.5.5

= 2.5.3 =
* Sortie : 10 septembre 2015

* Mise à jour traduction pour Divi 2.5.3
* Améliorations de la traduction du Builder
* Corrections erreurs typos

= 2.4.6.4 =
* Sortie : 20 aout 2015

* Mise à jour traduction pour Divi 2.4.6.4
* Améliorations de la traduction du builder
* Corrections erreurs typos

= 2.4.6.1 =
* Sortie :19 juillet 2015

* Correction du bug de la redéclaration function de Divi Child

= 2.4.6 =
* Sortie : 18 juillet 2015

* Mise à jour traduction pour Divi 2.4.6
* Ajout traduction pour le Divi Builder
* Ajout fallback traduction dans le repertoire languages/plugins de WordPress
* Ajout fallback traduction de Divi Child
* Suppression du fallback de chargement des traductions de Divi
* Testé pour WP 4.3

= 2.2.1 =
* Sortie : 21 décembre 2014

* Résolution bug auto-download
* Testé pour  WP 4.1

= Version 2.2 =
* Sortie : 15 novembre 2014

* Mise à jour traduction pour Divi 2.2

= Version 2.1.4 =
* Sortie : 10 novembre 2014

* Mise à jour traduction pour Divi 2.1.4

= 1.0.1 =
* Sortie : 5 novembre 2014

* Correction de l’erreur de l'auto-update - EDD_SL v3.0.2
* Correction du bug de l’affichage du changelog

= 1.0 =
* Sortie : 1 novembre 2014

* I Speak WordPress - Je parle français
* Mise en ligne

= 0.1 =
* Sortie: octobre 2014

* Le code est tout chaud