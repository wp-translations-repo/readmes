=== Divi Builder French ===
Contributors: WP-Translations Team, G3ronim0, fxbenard
Tags: i18n, l10n, Divi
Requires at least: 4.1
Tested up to: 4.9.1
Stable tag: 2.0.55
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Divi Builder French, la traduction française pour Divi Builder

== Description ==

Divi Builder French est la traduction française pour [Divi Builder](https://fxbenard.com/recommande/divi/). Divi Builder French vous permet grâce à son système de licence et de Language Pack de bénéficier en toute tranquilité et liberté de la mise à jour des traductions.

**Related Links:**

* https://shop.wp-translations.pro/


== Support ==

Pour toutes suggestions, améliorations ou demandes de corrections, n’hésitez pas à utiliser [notre page de contact](https://shop.wp-translations.pro/contact/).
Ou directement par e-mail : wpt@wp-translations.pro.


== Installation ==

1. Téléchargez le fichier zip de l’extension.
2. Installez et activez l’extension en cliquant sur via « Extensions » > « Ajouter ».
3. Saississez et activez votre licence dans le menu « WPT Pro » > « Divi Builder French ».
4. Voilà, votre thème est traduit et prêt à recevoir les prochaines mises à jour de ses traductions !


== Changelog ==

= 2.0.55 - 2018-01-04 =
* Mise à jour pour Divi Builder 2.0.55 - WordPress 4..9.1
* Refonte complète des traductions du Builder - Termes officiels de WordPress
* Correction typographiques - Espaces insécables

= 1.3.5 - 2016-05-19 =
* Mise à jour pour Divi Builder 1.3.6 - WordPress 4.5.2
* Correction du bug du non chargement des trads du Builder
* Refonte complète des traductions du Builder - Termes officiels de WordPress
* Correction typographiques - Espaces insécables
* Suppression de la traduction de et_automatic_updates
* Traduction onglet mises à jour

= 1.3.3 2016-05-03 =
* Mise à jour pour Divi Builder 1.3.3
* Traduction de l’onglet Mises à jour
* Suppression de la traduction pour ET Automatic Updates

= 1.3.1 - 2016-04-04 =
* Mise à jour pour Divi Builder 1.3.1
* Correction erreur duplicata nom de function

= 1.3.0 - 2016-04-01 =
* Mise à jour pour Divi Builder 1.3
* Amélioration - Traductions personnalisées dans  WP_LANG_DIR/fx-trads/
* Testé pour WordPress 4.5

= 1.2.4.4 - 216-03-29 =
* Numéro version identique theme source
* Amélioration - Numéro de  licence masqué dans l’interface

= 1.2.4.2 - 2016-02-22 =
* Number version identique extension source
* Ajout traduction ET Updater Plugin

= 1.0.0 - 2016-02 =
* I Speak WordPress - Je parle français

