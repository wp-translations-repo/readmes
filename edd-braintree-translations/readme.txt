=== EDD Braintree Translations ===
Contributors: fxbenard
Tags: ecommerce, edd, french, français, i18n, easy digital downloads, digital downloads, e-downloads, e-commerce, fxbenard
Requires at least: 3.8
Tested up to: 4.2
Stable tag: 1.1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Traductions françaises pour l'extension EDD Braintree

== Description ==

EDD Braintree Translations est une extension de traduction payante pour l'extension [EDD Braintree](https://easydigitaldownloads.com/extensions/braintree-gateway/). Cette extension ne fonctionnera que si vous l'avez au préalablement installée.


= Support =

Pour toutes suggestions, améliorations ou demandes de corrections, n'hésitez pas à utiliser [ma page de contact](https://fxbenard.com/contact/).
Ou contactez-moi directement par e-mail : fx@fxbenard.com.


== Installation ==

= Depuis votre site WordPress  =
Direction Extension > Ajouter > Mettre une extension en ligne > Selectionner le fichier zip préalablement téléchargé > Installer maintenant et tada !

= Installation manuelle =
Extraire le fichier zip et déposez-le dans votre dossier wp-content/plugins/ de votre installation WordPress et activez-le depuis votre page extension.


== Changelog ==

= 1.1 (25 mars 2015) =
* Numéro version identique à EDD Braintree

= 1.0.1 (23 mars 2015) =
* Ajout notification si EDD pas installé

= 1.0 (20 mars 2015) =
* Le code est tout chaud