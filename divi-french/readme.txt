=== Divi French ===
Contributors: WP-Translations Team, G3ronim0, fxbenard
Tags: i18n, l10n, Divi
Requires at least: 4.1
Tested up to: 4.9.4
Stable tag: 3.0.105
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Divi French, la traduction française pour Divi

== Description ==

Divi French est la traduction française pour [Divi](https://fxbenard.com/recommande/divi/). Divi French vous permet grâce à son système de licence et de Language Pack de bénéficier en toute tranquilité et liberté de la mise à jour des traductions.

**Related Links:**

* https://wp-translations.store/fr/downloads/divi-french/


== Support ==

Pour toutes suggestions, améliorations ou demandes de corrections, n’hésitez pas à utiliser [notre page de contact](https://shop.wp-translations.pro/contact/).
Ou directement par e-mail : wpt@wp-translations.pro.


== Installation ==

1. Téléchargez le fichier zip de l’extension.
2. Installez et activez l’extension en cliquant sur via « Extensions » > « Ajouter ».
3. Saississez et activez votre licence dans le menu « WPT Pro » > « Divi French ».
4. Voilà, votre thème est traduit et prêt à recevoir les prochaines mises à jour de ses traductions !


== Changelog ==

= 3.0.105 - 2018-02-19 =
* Mise à jour pour Divi < 3.0.105

= 3.0.101 - 2018-02-13 =
* Mise à jour pour Divi < 3.0.101
* Testé pour WordPress 4.9.4
* Ajout des séparateurs de sections dynamiques
* Ajout des traductions des vidéos d’aide

= 3.0.99 - 2018-02-01 =
* Mise à jour pour Divi < 3.0.99
* Ajout des 150+ mises en page au Divi Builder

= 3.0.98 - 2018-01-31 =
* Mise à jour pour Divi < 3.0.98

= 3.0.95 - 2018-01-11 =
* Mise à jour pour Divi < 3.0.95
* Ajout traductions Test A/B au builder

= 3.0.93 - 2018-01-04 =
* Mise à jour pour Divi < 3.0.93
* Amélioration espaces insécables

= 3.0.92 - 2017-12-19 =
* Mise à jour pour Divi < 3.0.92

= 3.0.90 - 2017-12-05 =
* Mise à jour pour Divi < 3.0.90
* Testé pour WordPress 4.9.1
* Introduction nouveaux termes du coeur de WP
* Amélioration en-tête modules
* Généralisation des ombres sur tous les modules

= 3.0.86 - 2017-11-06 =
* Mise à jour pour Divi < 3.0.86
* Testé pour WordPress 4.8.3

= 3.0.85 - 2017-10-27 =
* Mise à jour pour Divi < 3.0.85

= 3.0.83 - 2017-10-25 =
* Mise à jour pour Divi < 3.0.83
* Refonte générale des contrôles
* Refonte des réglages du Builder Visuel
* Refonte module texte
* Refonte des réglages des ombres

= 3.0.76 - 25 septembre 2017 =
* Mise à jour pour Divi < 3.0.69
* Testé pour WordPress 4.8.2
* Refonte générale des modules
* Refonte des réglages
* Refonte module abonnement

= 3.0.69 - 24 aout 2017 =
* Mise à jour pour Divi 3.0.69
* Testé pour WordPress 4.8.1
* Ajout traductions pour la visite guidée
* Correction du problème d’affichage du nombre de commentaires

= 3.0.63 - 18 juillet 2017 =
* Mise à jour pour Divi 3.0.63

= 3.0.51 - 26 juin 2017 =
* Mise à jour pour Divi 3.0.51
* Testé pour WordPress 4.8

= 3.0.42 - 03 mai 2017 =
* Mise à jour pour Divi 3.0.42

= 3.0.40 - 21 avril 2017 =
* Mise à jour pour Divi 3.0.40
* Testé pour WordPress 4.7.4

= 3.0.39 - 13 avril 2017 =
* Mise à jour pour Divi 3.0.39
* Testé pour WordPress 4.7.3
* Amélioration affichage de la recherche
* Amélioration traductions de la Bibliothèque

= 3.0.35 - 28 mars 2017 =
* Suppression ancien fix inutile avec Divi v.3.0.35

= 3.0.34 - 11 février 2017 =
* Mise à jour pour Divi 3.0.34

= 3.0.29 - 23 janvier 2017 =
* Mise à jour pour Divi 3.0.29

= 3.0.24 - 19 décembre 2016 =
* Mise à jour pour Divi 3.0.24

= 3.0.23 - 11 décembre 2016 =
* Mise à jour pour Divi 3.0.23
* Testé pour WordPress 4.7
* Bonus Divi-Child v.3.0.23

= 3.0.19 - 18 novembre 2016 =
* Mise à jour pour Divi 3.0.19
* Correction traduction des réglages du thème

= 3.0.15 - 20 octobre 2016 =
* Mise à jour pour Divi 3.0.15
* Refonte traduction des réglages du thème
* Simplification de la personnalisation

= 3.0.14 - 12 octobre 2016 =
* Mise à jour pour Divi 3.0.14

= 3.0.9 - 28 septembre 2016 =
* Mise à jour pour Divi 3.0.9

= 3.0.8 - 25 septembre 2016 =
* Mise à jour pour Divi 3.0.8
* Ajout traductions Builder Visuel

= 3.0.7 - 20 septembre 2016 =
* Mise à jour pour Divi 3.0.7
* Amélioration des règles CSS

= 3.0.6 - 18 septembre 2016 =
* Mise à jour pour Divi 3.0.6
* Ajout CSS pour meilleure affichage du Builder

= 3.0.5 - 16 septembre 2016 =
* Mise à jour pour Divi 3.0.5
* Amélioration traductions A/B Section
* Retour des majuscules sur les modules

= 3.0.1 - 09 septembre 2016 =
* Mise à jour pour Divi 3.0.1

= 3.0.0 - 07 septembre 2016 =
* Mise à jour pour Divi 3.0.0 - WordPress 4.6.1
* Amélioration traductions (props @nicolasricher)

= 2.7.8 - 06 juillet 2016 =
* Mise à jour pour Divi 2.7.8 - WordPress 4.5.3
* Amélioration traductions (props @nicolasricher)

= 2.7.5.1 - 27 mai 2016 =
* Correction du problème d’enregistrement dans l’outil de personnalisation

= 2.7.5 - 19 mai 2016 =
* Mise à jour pour Divi 2.7.5 - WordPress 4.5.2
* Correction du bug du non chargement des trads du Builder
* Refonte complète des traductions du Builder - Termes officiels de WordPress
* Correction typographiques - Espaces insécables
* Suppression de la traduction de et_automatic_updates
* Traduction onglet mises à jour

= 2.7.3 - 3 mai 2016 =
* Mise à jour pour Divi 2.7.3

= 2.7.1 - 4 avril 2016 =
* Mise à jour pour Divi 2.7.1

= 2.7.0 - 1 avril 2016 =
* Mise à jour pour Divi 2.7
* Amélioration - Traductions personnalisées dans  WP_LANG_DIR/fx-trads/

= 2.6.4.4 - 29 mars 2016 =
* Mise à jour  pour Divi 2.6.4.4
* Amélioration - Numéro de  licence masqué dans l’interface
* Amélioration traductions du e-Panel (props @nicolasricher)

= 2.6.4.2 - 22 février 2016 =
* Mise à jour  pour Divi 2.6.4.2
* Insertion traduction pour Elegant Themes updater

= 2.6.4.1 - 21 février 2016 =
* Mise à jour  pour Divi 2.6.4.1

= 2.6.1 - 27 janvier 2016 =
* Mise à jour  pour Divi 2.6.1

= 2.6.0 - 26 janvier 2016 =
* Mise à jour traduction pour Divi 2.6.0
* +200 nouvelles traductions du Builder/Divi
* Refonte interface merci @G3ronim0
* Testé pour WP 4.4.1

= 2.5.5 - 11 novembre 2015 =
* Mise à jour traduction pour Divi 2.5.5

= 2.5.3 - 10 septembre 2015 =
* Mise à jour traduction pour Divi 2.5.3
* Améliorations de la traduction du Builder
* Corrections erreurs typos

= 2.4.6.4 - 20 aout 2015 =
* Mise à jour traduction pour Divi 2.4.6.4
* Améliorations de la traduction du builder
* Corrections erreurs typos

= 2.4.6.1 - 19 juillet 2015 =
* Correction du bug de la redéclaration function de Divi Child

= 2.4.6 - 18 juillet 2015 =
* Mise à jour traduction pour Divi 2.4.6
* Ajout traduction pour le Divi Builder
* Ajout fallback traduction dans le repertoire languages/plugins de WordPress
* Ajout fallback traduction de Divi Child
* Suppression du fallback de chargement des traductions de Divi
* Testé pour WP 4.3

= 2.2.1 - 21 décembre 2014 =
* Résolution bug auto-download
* Testé pour  WP 4.1

= Version 2.2 - 15 novembre 2014 =
* Mise à jour traduction pour Divi 2.2

= Version 2.1.4 - 10 novembre 2014 =
* Mise à jour traduction pour Divi 2.1.4

= 1.0.1 - 5 novembre 2014 =
* Correction de l’erreur de l'auto-update - EDD_SL v3.0.2
* Correction du bug de l’affichage du changelog

= 1.0 - 1 novembre 2014 =
* I Speak WordPress - Je parle français
* Mise en ligne

= 0.1 - octobre 2014 =
* Le code est tout chaud
