=== EDD Free Downloads Translations ===
Contributors: fxbenard
Tags: ecommerce, edd, french, français, i18n, easy digital downloads, digital downloads, e-downloads, e-commerce, fxbenard
Requires at least: 3.8
Tested up to: 4.2
Stable tag: 1.0.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Traductions françaises pour l'extension EDD Free Downloads

== Description ==

EDD Free Downloads French est une extension de traduction payante pour l'extension [EDD Free Downloads](https://easydigitaldownloads.com/extensions/free-downloads/). Cette extension ne fonctionnera que si vous l'avez au préalablement installée.


== Support ==

Pour toutes suggestions, améliorations ou demandes de corrections, n'hésitez pas à utiliser [ma page de contact](https://fxbenard.com/contact/).
Ou directement par e-mail : fx@fxbenard.com.


== Installation ==

= Depuis votre site WordPress  =
Direction Extension > Ajouter > Mettre une extension en ligne > Selectionner le fichier zip préalablement téléchargé > Installer maintenant et tada !

= Installation manuelle =
Extraire le fichier zip et déposez-le dans votre dossier wp-content/plugins/ de votre installation WordPress et activez-le depuis votre page extension.


== Changelog ==

= 1.0.5 (18 mai 2015) =
* Numéro version identique à EDD Free Downloads

= 1.0.0 (13 mai 2015) =
* Disponible

= 0.0.1 (mai 2015) =
* Le code est tout chaud