=== Extra French ===
Contributors: WP-Translations Team, G3ronim0, fxbenard
Tags: français, fxbenard, Extra
Requires at least: 4.1
Tested up to: 4.9.4
Stable tag: 2.0.104
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Extra French, la traduction française pour Extra


== Description ==

Extra French, la traduction française pour [Extra](https://fxbenard.com/recommande/extra). Extra French vous permet grâce à son système de licence et de paquet de langues intégrés de bénéficier en toute tranquilité et liberté des mises à jour des traductions.

**Liens relatifs**

[Page du store](https://wp-translations.store/fr/downloads/extra-french/)


== Support ==

Pour toutes suggestions, améliorations ou demandes de corrections, n’hésitez pas à utiliser [notre page de contact](https://wp-translations.store/contact/).
Ou directement par e-mail : wpt@wp-translations.pro.


== Installation ==

1. Téléchargez le fichier zip de l’extension.
2. Installez et activez l’extension en cliquant sur via « Extensions » > « Ajouter ».
3. Saississez et activez votre licence dans le menu « WPT Pro » > « Licences » > « Extra French ».
4. Voilà, votre thème est traduit et prêt à recevoir les prochaines mises à jour de ses traductions !


== Changelog ==

= 2.0.104 - 2018-02-20 =
* Mise à jour pour Extra < 2.0.104

= 2.0.100 - 2018-02-14 =
* Mise à jour pour Extra < 2.0.100
* Testé pour WordPress 4.9.4
* Ajout des séparateurs de sections dynamiques
* Ajout des traductions des vidéos d’aide

= 2.0.98 - 2018-02-02 =
* Mise à jour pour Extra < 2.0.98
* Ajout section vidéos
* Ajout section mises en page pré-conçues

= 2.0.94 - 2018-01-16 =
* Mise à jour pour Extra < 2.0.94
* Ajout traductions Test A/B au builder

= 2.0.93 - 2018-01-04 =
* Mise à jour pour Extra < 2.0.93
* Amélioration espaces insécables

= 2.0.92 - 2017-12-19 =
* Mise à jour pour Extra < 2.0.92

= 2.0.91 - 2017-12-19 =
* Mise à jour pour Extra < 2.0.91
* Testé pour WordPress 4.9.1
* Introduction nouveaux termes du coeur de WP
* Amélioration en-tête modules
* Généralisation des ombres sur tous les modules

= 2.0.82 - 2017-10-25 =
* Mise à jour pour Extra < 2.0.82
* Refonte générale des contrôles
* Refonte des réglages du Builder Visuel
* Refonte module texte
* Refonte des réglages des ombres

= 2.0.62 - 2017-07-18 =
* Mise à jour pour Extra 2.0.62
* Testé pour WordPress 4.8
* Nouvelles chaines dans le builder

= 2.0.46 (30 mai 2017) =
* Mise à jour pour Extra 2.0.46
* Testé pour WordPress 4.7.5
* Nouvelles chaines dans le builder
* Suppression ancien fix inutile avec Extra v.2.0.46

= 2.0.41 (03 mai 2017) =
* Mise à jour pour Extra 2.0.41

= 2.0.40 (30 avril 2017) =
* Mise à jour pour Extra 2.0.40
* Nouvelles chaines dans le builder

= 2.0.39 (24 avril 2017) =
* Mise à jour pour Extra 2.0.39
* Testé pour WordPress 4.7.4
* Correction du bug de chargement de certaines traductions

= 2.0.33.1 (04 mars 2017) =
* Correction du bug de chargement de certaines traductions

= 2.0.33 (14 février 2017) =
* Mise à jour pour Extra 2.0.33
* Testé pour WordPress 4.7.2

= 2.0.23 (19 décembre 2016) =
* Mise à jour pour Extra 2.0.23
* Testé pour WordPress 4.7
* Bonus extra-child v.2.0.23

= 2.0.18 (18 novembre 2016) =
* Mise à jour pour Extra 2.0.18
* Amélioration traduction des réglages du thème

= 2.0.14 (20 octobre 2016) =
* Mise à jour pour Extra 2.0.14
* Refonte traduction des réglages du thème
* Simplification de la personnalisation

= 2.0.13 (12 octobre 2016) =
* Mise à jour pour Extra 2.0.13

= 2.0.8 (28 septembre 2016) =
* Mise à jour pour Extra 2.0.8

= 2.0.6 (23 septembre 2016) =
* Mise à jour pour Extra 2.0.6
* Amélioration des règles CSS
* Correction du bug de chargement de certaines traductions

= 2.0.5 (19 septembre 2016) =
* Mise à jour pour Extra 2.0.5
* Ajout CSS pour meilleure affichage du Builder

= 2.0.4 (16 septembre 2016) =
* Mise à jour pour Extra 2.0.4 - WordPress 4.6.1

= 2.0.3 (14 septembre 2016) =
* Mise à jour pour Extra 2.0.3 - WordPress 4.6.1
* Prêt pour le Builder Visuel
* Retour des majuscules sur les modules

= 1.3.9 (06 juillet 2016) =
* Mise à jour pour Extra 1.3.9 - WordPress 4.5.3
* Nouvelles chaines traduites
* Nouvelle version d’Extra Child 1.3.9
* Correction du bug du chargement CSS d’Extra Child

= 1.3.6.2 (01 juin 2016) =
* Correction du problème d’incompatibilité Mac/Windows

= 1.3.6.1 (30 mai 2016) =
* Correction du problème d’enregistrement dans l’outil de personnalisation

= 1.3.6 (18 mai 2016) =
* Mise à jour pour Extra 1.3.6 - WordPress 4.5.2
* Correction du bug du non chargement des trads du Builder
* Refonte complète des traductions du Builder - Termes officiels de WordPress
* Correction typographiques - Espaces insécables
* Suppression de la traduction de et_automatic_updates
* Traduction onglet mises à jour

= 1.3.4 (3 mai 2016) =
* Mise à jour pour Extra 1.3.4
* Traduction de l’onglet Mises à jour

= 1.3.1 (4 avril 2016) =
* Mise à jour pour Extra 1.3.1
* Correction majuscule nom dossier qui ne fonctionnait pas correctement sur Mac

= 1.3.0 (1 avril 2016) =
* Mise à jour pour Extra 1.3
* Amélioration - Traductions personnalisées dans  WP_LANG_DIR/fx-trads/
* Testé pour WordPress 4.5

= 1.2.4.4 (29 mars 2016) =
* Numéro version identique theme source
* Amélioration - Numéro de  licence masqué dans l’interface
* Amélioration traductions du e-Panel (props @nicolasricher)

= 1.2.4.2 (22 février 2016) =
* Numéro version identique theme source
* Ajout traduction Et Updater Plugin

= 1.0.0 (03 février 2016) =
* I Speak WordPress - Je parle français

= 0.0.1 (février 2016) =
* Le code est tout chaud