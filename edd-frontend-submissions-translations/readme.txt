=== Easy Digital Downloads - Frontend Submissions Translations ===
Contributors: fxbenard, G3ronim0
Tags: français, fxbenard
Requires at least: 3.8
Tested up to: 4.4.2
Stable tag: 2.3.11
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Frontend Submissions Translations est une extension de traduction pour EDD Frontend Submissions

== Description ==

Frontend Submissions Translations est une extension de traduction payante pour l’extension [EDD  Frontend Submissions](https://easydigitaldownloads.com/extensions/frontend-submissions/). Cette extension ne fonctionnera que si vous l’avez au préalablement installée.

== Support ==

Pour toutes suggestions, améliorations ou demandes de corrections, n’hésitez pas à utiliser [ma page de contact](https://fxbenard.com/contactez-moi/).
Ou directement par e-mail : fx@fxbenard.com.


== Installation ==

= Depuis votre site WordPress  =
Direction Extension > Ajouter > Mettre une extension en ligne > Selectionner le fichier zip préalablement téléchargé > Installer maintenant et tada !

= Installation manuelle =
Extraire le fichier zip et déposez-le dans votre dossier wp-content/plugins/ de votre installation WordPress et activez-le depuis votre page Extension.


== Changelog ==

= 2.3.11 (14 mars 2016) =
* Refonte traduction pour EDD  Frontend Submissions 2.3

= 2.2.17 (6 mai 2015) =
* Numéro version identique à EDD  Frontend Submissions

= 1.0.0 (1 mai 2015) =
* Disponible

= 0.0.1 (mai 2015) =
* Le code est tout chaud