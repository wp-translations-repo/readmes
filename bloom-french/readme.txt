=== Bloom French ===
Contributors: WP-Translations Team, G3ronim0, fxbenard
Tags: i18n, l10n, WP-T
Requires at least: 4.1
Tested up to: 4.9.4
Stable tag: 1.2.23
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Bloom French, la traduction française pour Bloom


== Description ==

Bloom French, la traduction française pour [Bloom](https://fxbenard.com/recommande/bloom/). Bloom French vous permet grâce à son système de licence et de paquet de langues intégrés de bénéficier en toute tranquilité et liberté des mises à jour des traductions.

**Liens relatifs**

[Page du store](https://wp-translations.store/fr/downloads/bloom-french/)


== Support ==

Pour toutes suggestions, améliorations ou demandes de corrections, n’hésitez pas à utiliser [notre page de contact](https://wp-translations.store/fr/contact/).
Ou directement par e-mail : wpt@wp-translations.store.


== Installation ==

Si vous n’avez jamais installé l’extension WP-Translations
1. Téléchargez le fichier zip de l’extension.
2. Installez et activez l’extension en cliquant sur « Extensions » > « Ajouter ».
3. Saississez et activez votre licence dans le menu « WP-T Pro » > « Licences » > « Bloom French ».
4. Voilà, votre produit WordPress est traduit et prêt à recevoir les prochaines mises à jour de ses traductions !

Si vous avez déjà installé l’extension WP-Translations
1. Saississez et activez votre licence dans le menu « WP-T Pro » > « Licences » > « Bloom French ».
2. Voilà, votre produit WordPress est traduit et prêt à recevoir les prochaines mises à jour de ses traductions !


== Changelog ==

= 1.2.23 - 2018-02-20 =
* Prêt pour le store de WP-Translations

= 1.0.0 - 2015-07-27 =
* Mise en ligne

= 0.0.1 2015-07 =
* Le code est tout chaud